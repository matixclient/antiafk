package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.module.killaura.TimeManager;
import de.paxii.clarinet.util.module.settings.ValueBase;

import java.util.UUID;

/**
 * Created by Lars on 20.05.2016.
 */
public class ModuleAntiAFK extends Module {
  private TimeManager timeManager;

  public ModuleAntiAFK() {
    super("AntiAFK", ModuleCategory.OTHER);

    this.setVersion("1.0");
    this.setBuildVersion(15801);

    this.timeManager = new TimeManager();
    this.getModuleValues().put("delay", new ValueBase("AntiAFK Delay", 5.0F, 1.0F, 10.0F, true));
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent event) {
    if (this.timeManager.sleep((long) this.getModuleValues().get("delay").getValue() * 60 * 1000)) {
      String randomCompletion = UUID.randomUUID().toString().split("-")[0];

      Chat.sendChatMessage("/" + randomCompletion);
      Chat.printClientMessage("Sent Tab Completion Packet to prevent AFK kick.");

      this.timeManager.updateLast();
    }

    this.timeManager.updateTimer();
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
